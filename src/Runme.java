
import java.util.ArrayList;
import java.util.List;

import models.Actor;
import models.RandomSolver01;
import models.RandomSolver02;
import models.RandomSolver03;

public class Runme {

	private static List<Actor> actors = new ArrayList<Actor>();
	private static final boolean DEBUGOFF = false;
	private static final boolean DEBUGON = true;
	
	public static void main(String[] args) {

		//Setup the actors
		actors.add( new Actor("Me",1));
		actors.add( new Actor("Janitor",5));
		actors.add( new Actor("Lab Assistant",2));
		actors.add( new Actor("Old Professor",10));
		
		
		// Pick a solver and work it out
		RandomSolver03 randomeSolver = new RandomSolver03(actors, 17,DEBUGOFF);
		
		randomeSolver.workitout();
		
		//Report!
		System.out.println(randomeSolver.reportTheResult());

	}

}
