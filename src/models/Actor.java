package models;

public class Actor {
	private String name;
	private Integer walkTime;
	
	private Boolean isOverTheBridge = false;
	
	public Actor(String name, Integer walkTime ) {
		this.name = name;
		this.walkTime = walkTime;
	}
	
	public void reset() {
		this.setIsOverTheBridge(false);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getWalkTime() {
		return walkTime;
	}
	
	public String getPossition() {
		if(this.getIsOverTheBridge()) {
			return " safely over the bridge.";
		}
		return " in danger of being eaten by zombies!";
	}

	public void setWalkTime(Integer walkTime) {
		this.walkTime = walkTime;
	}

	public Boolean getIsOverTheBridge() {
		return isOverTheBridge;
	}

	public void setIsOverTheBridge(Boolean isOverTheBridge) {
		this.isOverTheBridge = isOverTheBridge;
	}
	
	
}
