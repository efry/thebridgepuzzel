package models;

import java.util.ArrayList;
import java.util.List;

/* Slight improvement to not move the last player that was moved again.  */
public class RandomSolver02 {

	/* Lets try solve the problem using bruit force until we get it! */
	
	private List<Actor> actors;
	private Integer maxSolveTime;
	private Integer lapsTime = 0;
	private boolean debug = true;
	private long gameCount = 0;
	private Integer lastActorMoved = -1;
	
	private List<String> steps = new ArrayList<String>();
	
	public RandomSolver02(List<Actor> actors, Integer maxSolveTime, boolean debug) {
		this.maxSolveTime = maxSolveTime;
		this.actors = actors;
		this.debug = debug;
		
		//make sure nothing else has been fiddling with our actors
		resetActors();
	}
	
	public void workitout() {
		
		/* Lets do it randomly in a dumb way without evolution */
		
		while(true) {
			
			if(debug) System.out.println("Lets try solve stuff!");
			
			
			while(lapsTime <= maxSolveTime) {
				
				if (gameIsSolved() ) { 
					if(debug) System.out.println("We found the solution!!!");
					return;
				} // yay!
				
				Actor actor = chooseAnActorRandomly();
				if(debug) System.out.println("We chose " + actor.getName() + " as the next randome player");
				
				moveActorAndIncreaseTime(actor);
				if(debug) System.out.println("Lapse time is " + lapsTime + " and last action : " + steps.get(steps.size() -1 ));
			}
			
			//We must have failed :( Try again!
			if(debug) System.out.println(reportTheResult());
			
			resetSolver();
			System.out.print("Try number " + ++gameCount + "\r");
		}
	}
	
	private Actor chooseAnActorRandomly() {
		
		int playerIndex = -1;
		
		do {
			playerIndex = (int) Math.round((Math.random() * (actors.size() - 1)) ) ;
		} while(playerIndex == lastActorMoved);
		
		lastActorMoved = playerIndex;
		return actors.get(playerIndex);
	}
	
	private void moveActorAndIncreaseTime(Actor actor) {
		actor.setIsOverTheBridge( ! actor.getIsOverTheBridge());
		this.steps.add(actor.getName() + " is now " + actor.getPossition());
		lapsTime += actor.getWalkTime();
	}
	
	private void resetActors() {
		actors.stream()
		.forEach(actor -> actor.reset());
	}
	
	private void resetSolver() {
		lapsTime = 0;
		resetActors();
		steps.clear();
	}
	
	public boolean gameIsSolved() {

		for( Actor actor : this.actors)  {
			
			if(lapsTime > maxSolveTime) {
				return false;
			}
			
			if(actor.getIsOverTheBridge() == false) {
				return false;
			}
		};
		
		return true;
	}
	
	public String reportTheResult() {
		
		String out = "Steps Taken\n";
		for(String step:steps) {
			out += step + "\n";
		}
		
		if(gameIsSolved() ) {
			out += "And you are all safe!\n";
		}  else {
			out += "And you have all been eaten by zombies!\n";
		}
		
		return out;
	}
}
