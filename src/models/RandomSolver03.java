package models;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/* Doh! Version 1 and 2 will never finish, we need to take 2 x actors over at once! 
 * This version actually works.
 */

public class RandomSolver03 {

	private List<Actor> actors;
	private Integer maxSolveTime;
	private Integer lapsTime = 0;
	private boolean debug = true;
	private long gameCount = 0;
	private List<Actor> lastSelectedActors = new ArrayList<Actor>();
	private List<Actor> selectedActors = new ArrayList<Actor>();
	private boolean lanternHasCrossed = false;
	
	private List<String> steps = new ArrayList<String>();
	
	public RandomSolver03(List<Actor> actors, Integer maxSolveTime, boolean debug) {
		
		this.maxSolveTime = maxSolveTime;
		this.actors = actors;
		this.debug = debug;
		
		//make sure nothing else has been fiddling with our actors
		resetActors();
	}
	
	public void workitout() {
		
		// do it forever or until its solved!
		while(true) {
			
			if(debug) System.out.println("Lets try solve stuff!");
			
			while(lapsTime <= maxSolveTime) {
				
				chooseAnActorsRandomly();
				
				if(debug) System.out.println("We chose " + lastSelectedActors.get(0) + " and " + lastSelectedActors.get(1) + " as the next randome players");
				
				moveActorAndIncreaseTime();
				if(debug) System.out.println("Lapse time is " + lapsTime + " and last action : " + steps.get(steps.size() -1 ));

				if (gameIsSolved() ) { 
					if(debug) System.out.println("We found the solution!!!");
					return;
				} // yay!

			}
			
			//We must have failed :( Try again!
			if(debug) System.out.println(reportTheResult());
			
			resetSolver();
			System.out.print("Try number " + ++gameCount + "\r");
		}
	}
	
	private void chooseAnActorsRandomly() {
		
		int playerIndex=-1;
		
		lastSelectedActors.clear();
		for(Actor actor : selectedActors) {
			lastSelectedActors.add(actor);
		}
		
		selectedActors.clear();
		
		List<Actor> actorsOnLanternSide = getListOfActorsOnTheLanturnSide();
		
		if (lanternHasCrossed) { //only ever send back one player
			
			playerIndex = (int) Math.round( Math.random() * (actorsOnLanternSide.size()-1));
			selectedActors.add(actorsOnLanternSide.get(playerIndex));
			
		} else {  // take two actors across the bridge 
			
			do {
				playerIndex = (int) Math.round( Math.random() * (actorsOnLanternSide.size()-1));
				if (!selectedActors.contains(actorsOnLanternSide.get(playerIndex)) ) {
					selectedActors.add(actorsOnLanternSide.get(playerIndex)); //is unique 
				}
			} while(selectedActors.size() < 2);
		}
	}
	
	private List<Actor> getListOfActorsOnTheLanturnSide() {

		return actors.stream()
			.filter(actor ->  actor.getIsOverTheBridge() == this.lanternHasCrossed )
			.collect(Collectors.toList());
	}
	
	private void moveActorAndIncreaseTime() {
		
		int slowestWalkerSpeed=-1;
		
		for ( Actor actor : selectedActors) {
			
			actor.setIsOverTheBridge( !actor.getIsOverTheBridge());
			
			if (slowestWalkerSpeed < actor.getWalkTime()) {
				slowestWalkerSpeed = actor.getWalkTime();
			}
		}

		lapsTime += slowestWalkerSpeed;

		if(selectedActors.size() > 1) {
			this.steps.add(selectedActors.get(0).getName() + " and " + selectedActors.get(1).getName() + " walked over the bridge");
		} else {
			this.steps.add(selectedActors.get(0).getName() + " walked over the bridge alone");
		}
		
		this.steps.add(this.asciiGameState());
		
		lanternHasCrossed = !lanternHasCrossed;
	}
	
	private void resetActors() {
		actors.stream()
		.forEach(actor -> actor.reset());
	}
	
	private void resetSolver() {
		lapsTime = 0;
		lanternHasCrossed = false;
		resetActors();
		steps.clear();
		lastSelectedActors.clear();
		selectedActors.clear();
	}
	
	public boolean gameIsSolved() {
		
		if(lapsTime > maxSolveTime) {
			return false;
		}
		
		for( Actor actor : this.actors)  {

			if(actor.getIsOverTheBridge() == false) { return false; }
		};
		
		return true;
	}
	
	public String reportTheResult() {
		
		String out = "Steps Taken\n";
		for(String step:steps) {
			out += step + "\n";
		}
		
		if(gameIsSolved() ) {
			out += "And you are all safe!\n";
		} else if (!gameIsSolved() && lapsTime <= maxSolveTime) {
			out += "And we are still playing with time elapsed "+lapsTime+"\n";
		}  else {
			out += "And you have all been eaten by zombies as laps time is "+lapsTime+"!\n";
		}
		
		return out;
	}
	
	private String asciiGameState() {
		
		//Represent zombies!
		String out = " /\\ \n";
		out       += "/\\ \\ ";
		
		//draw zomebies
		for(int x = 0; x <= maxSolveTime ; x++) {
			
			if(lapsTime < x) {
				out += "_";
			}
			
			if(lapsTime == x ) {
				out += "Z";
			}
			
			if (lapsTime > x) {
				out +="z";
			}
		}
		
		out += " ";
		
		//draw players on this side of bridge
		String otherside = "";
		for(Actor actor: actors) {
			if(actor.getIsOverTheBridge() == false ) {
				out += actor.getName().substring(0,1);
			} else {
				otherside += actor.getName().substring(0,1);
			}
		}
		
		out+= " ]\\===================/[ ";
		out+= otherside;
		
		out+= "___ Time: " +lapsTime+"\n";
		
		return out;
	}
}
