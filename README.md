# README #

Here is my attempt at coding the Zombie puzzle programatically. You can find the riddle and solution here https://youtu.be/7yDmGnA8Hw0

The point of this exercise was not to solve the puzzle but rather learn coding and solve the problem of programatically finding the solution.

The method employed here is pretty simple and clumsy and goes something like this...

1) Choose two players randomly from the lantern side and move them over the bridge.

2) Move only one player back across the bridge

3) Goto 1 until we are out of time or solved the puzzle

4) Reset game and try again if we failed.

5) If we succeeded, print out moves and live zomebie free lives!

I have various failed attempts but the main bit of code can be found in RandomSolver03

Enjoy!